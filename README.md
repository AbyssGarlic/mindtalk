# README #

My mind wanders when I'm awake/
The ghosts won't rest until I fall asleep/
I need a place to settle myself in/
A place I can never find in a dream/
The ghosts capture me by a whisper/
They will torture me and make me squeal/
